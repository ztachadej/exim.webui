﻿import BasePageHandler from '../../src/component/basepagehandler';
import HomeService from "../../src/services/homeservice";
import { dataModel } from "../../src/models/homemodel";
export default class Home extends BasePageHandler {
    constructor(resolve) {
        super();
        this.HomeService = new HomeService();
        this.initPage();
        this.loadPage(resolve);
    }
    initPage() {
        this.phonetxt = new this.phoneField("phone").bind();
        this.date = new this.datepicker("stdate", null, (o) => { console.log(o.element.val()); }).bind();
        this.dd = new this.dropdown("cb_info", [{ text: "select 1", value: "value1" }, { text: "select 2", value: "value2" }], { allow_single_deselect: true }).bind();
        this.validate({
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true,
                    phone: true
                },
                cb_info: "required",
                stdate: "required"
            },
            messages: {
                name: this.commonUtils.getMessageLang(this.messageLang.requireFirstName),
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                },
                phone: {
                    required: "Please specify your phone number",
                    phone: "Please enter a valid phone numberxxx"
                },
                cb_info: this.commonUtils.getMessageLang(this.messageLang.requireFirstName),
                stdate: this.commonUtils.getMessageLang(this.messageLang.requireFirstName)
            }

        });
        $("#btnLoad").click(() => {
            dataModel.name = "Tachadej";
            dataModel.email = "tachadej@gmail.com";
            dataModel.phone = "0954916252";
            dataModel.stdate = "04/03/2019 11:18";
            dataModel.cb_info = "value1";
            dataModel.chk = true;
            dataModel.rdo1 = true;
            this.setPageData(dataModel);
        });
        $("#btnSave").click(() => {
            if (this.valid()) {
                console.log("Success");
            }
        });
        $('#example-getting-started').multiselect({
            buttonWidth: '100%', enableFiltering: true, enableCaseInsensitiveFiltering: true,
            includeSelectAllOption: true
        });
    }
    async loadPage(resolve) {
        //this.dd = new this.dropdown("cb_info", [{ text: "select 1", value: "value 1" }, { text: "select 2", value: "value 2" }], { allow_single_deselect: true }).bind();
        //this.date.setMinDate(new Date());
        //this.numfield = new this.numberField("numfield", { decimal: 2 }).bind();
        this.table = new this.datatable("tbl1", {
            "data": [
                {
                    "id": "1",
                    "name": "Tiger Nixon",
                    "position": "System Architect",
                    "salary": "$320,800",
                    "start_date": "2011/04/25",
                    "office": "Edinburgh",
                    "extn": "5421"
                },
                {
                    "id": "2",
                    "name": "Garrett Winters",
                    "position": "Accountant",
                    "salary": "$170,750",
                    "start_date": "2011/07/25",
                    "office": "Tokyo",
                    "extn": "8422"
                },
                {
                    "id": "3",
                    "name": "Ashton Cox",
                    "position": "Junior Technical Author",
                    "salary": "$86,000",
                    "start_date": "2009/01/12",
                    "office": "San Francisco",
                    "extn": "1562"
                },
                {
                    "id": "4",
                    "name": "Cedric Kelly",
                    "position": "Senior Javascript Developer",
                    "salary": "$433,060",
                    "start_date": "2012/03/29",
                    "office": "Edinburgh",
                    "extn": "6224"
                }
            ],
            "columns": [
                { "data": "name" },
                { "data": "position" },
                { "data": "office" },
                { "data": "salary" }
            ]
        }).bind();
        this.tablegrp = new this.datatablegroup("tblgrp", {
            "data": [
                {
                    "id": "1",
                    "name": "Tiger Nixon",
                    "position": "System Architect",
                    "salary": "$320,800",
                    "start_date": "2011/04/25",
                    "office": "Edinburgh",
                    "extn": "5421"
                },
                {
                    "id": "2",
                    "name": "Garrett Winters",
                    "position": "Accountant",
                    "salary": "$170,750",
                    "start_date": "2011/07/25",
                    "office": "Tokyo",
                    "extn": "8422"
                },
                {
                    "id": "3",
                    "name": "Ashton Cox",
                    "position": "Junior Technical Author",
                    "salary": "$86,000",
                    "start_date": "2009/01/12",
                    "office": "San Francisco",
                    "extn": "1562"
                },
                {
                    "id": "4",
                    "name": "Cedric Kelly",
                    "position": "Senior Javascript Developer",
                    "salary": "$433,060",
                    "start_date": "2012/03/29",
                    "office": "Edinburgh",
                    "extn": "6224"
                },
                {
                    "id": "5",
                    "name": "Airi Satou",
                    "position": "Accountant",
                    "salary": "$162,700",
                    "start_date": "2008/11/28",
                    "office": "Tokyo",
                    "extn": "5407"
                },
                {
                    "id": "6",
                    "name": "Brielle Williamson",
                    "position": "Integration Specialist",
                    "salary": "$372,000",
                    "start_date": "2012/12/02",
                    "office": "New York",
                    "extn": "4804"
                },
                {
                    "id": "7",
                    "name": "Herrod Chandler",
                    "position": "Sales Assistant",
                    "salary": "$137,500",
                    "start_date": "2012/08/06",
                    "office": "San Francisco",
                    "extn": "9608"
                },
                {
                    "id": "8",
                    "name": "Rhona Davidson",
                    "position": "Integration Specialist",
                    "salary": "$327,900",
                    "start_date": "2010/10/14",
                    "office": "Tokyo",
                    "extn": "6200"
                },
                {
                    "id": "9",
                    "name": "Colleen Hurst",
                    "position": "Javascript Developer",
                    "salary": "$205,500",
                    "start_date": "2009/09/15",
                    "office": "San Francisco",
                    "extn": "2360"
                },
                {
                    "id": "10",
                    "name": "Sonya Frost",
                    "position": "Software Engineer",
                    "salary": "$103,600",
                    "start_date": "2008/12/13",
                    "office": "Edinburgh",
                    "extn": "1667"
                },
                {
                    "id": "11",
                    "name": "Jena Gaines",
                    "position": "Office Manager",
                    "salary": "$90,560",
                    "start_date": "2008/12/19",
                    "office": "London",
                    "extn": "3814"
                },
                {
                    "id": "12",
                    "name": "Quinn Flynn",
                    "position": "Support Lead",
                    "salary": "$342,000",
                    "start_date": "2013/03/03",
                    "office": "Edinburgh",
                    "extn": "9497"
                }
            ],
            "columns": [
                { "data": "name" },
                { "data": "position" },
                { "data": "office" },
                { "data": "salary" }
            ],
            "child": this.format
        }).bind();
        const call1 = this.HomeService.getHome();
        const call2 = this.HomeService.getHome();
        const ret = await Promise.all([call1, call2]);
        console.log(ret);
        const call3 = await this.HomeService.getHome();
        console.log("33333333333333");
        console.log(call3);
        resolve();
    }
    format(d) {
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
            '<tr>' +
            '<td>Full name:</td>' +
            '<td>' + d.name + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extension number:</td>' +
            '<td>' + d.extn + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Extra info:</td>' +
            '<td>And any further details here (images etc)...</td>' +
            '</tr>' +
            '</table>';
    }
    async getData() {

    }
}
window.pageLoad = resolve => new Home(resolve);
