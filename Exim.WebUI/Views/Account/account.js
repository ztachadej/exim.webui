﻿import './account.css';
import 'babel-polyfill';
import langUtils from "../../src/common/langUtils";
import BasePageHandler from '../../src/component/basepagehandler';
import authentication from "../../src/common/authentication";
import { status } from "../../src/config/api";
import AccountService from "../../src/services/accountservice";
export default class Login extends BasePageHandler {
    constructor() {
        super();
        this.AccountService = new AccountService();
        this.loadPage();
    }
    loadPage() {
        $("#loginbtn").on('click', () => {
            if (this.formUtils.validate()) {
                this.login();
            }
        });
        $("#langtxt").change((o) => {
            window.lang.change(o.target.value);
        });
        this.formUtils.hideLoading();
    }
    loginResult(resp) {
        if (resp) {
            if (resp.status === status.OK) {
                authentication.setSessionData(resp.result);
                this.commonUtils.redirectTo(`${this.commonUtils.getHostUrl()}/Home/Index`);
            }
            else {
                this.formUtils.alertFailed("Username or password is incorrect");
            }
        }
        else
            authentication.removeSession();
    }
    async login() {
        authentication.setSessionData({ current_lang: $("#langtxt").val() });
        let result = await this.AccountService.login($("#usernametxt").val(), $("#passwordtxt").val());
        this.loginResult(result);
    }
}
langUtils.initialize();
$(document).ready(() => {
    new Login();
});