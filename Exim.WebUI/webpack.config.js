﻿const path = require('path');
const webpack = require('webpack');
var glob = require("glob");
var entryObject = glob.sync('./Views/**/*.js').reduce(
    function (entries, entry) {
        var matchForRename = /^\.\/Views\/([\w\d_]+)\/.+\.js$/g.exec(entry);
        if (matchForRename !== null && typeof matchForRename[1] !== 'undefined') {
            var entryName = "views/js/" + matchForRename[1].toLocaleLowerCase();
            if (typeof entries[entryName] !== 'undefined') {
                entries[entryName].push(entry);
            } else {
                entries[entryName] = [entry];
            }
        }
        return entries;
    },
    {}
);
entryObject["main"] = ["babel-polyfill","./src/main"];
entryObject["jquery"] = "./src/jquery";
module.exports = {
    entry: entryObject,
    output: {
        path: path.join(__dirname, "/wwwroot/js/"),
        filename: "[name].build.js"
    },
    module: {
        rules: [
            {
                test: /\.css$/, use: [{ loader: "style-loader" },
                { loader: "css-loader" }]
            },
            {
                test: /\.js?$/,
                use: {
                    loader: 'babel-loader', options: {
                        presets:
                            ['@babel/preset-env']
                    }
                }
            },
        ]
    }
};