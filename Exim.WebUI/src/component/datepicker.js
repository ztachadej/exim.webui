﻿import { datetimeformat, dateformat } from "../config/config";
import authentication from "../common/authentication";
export default class Datepicker {
    constructor(id, option, fn) {
        this.id = id;
        this.element = $("#" + id);
        this.option = option;
        if (!this.option) {
            this.option = {
                format: datetimeformat
            };
        }
        else {
            if (this.option.dateOnly) {
                this.option.format = dateformat;
            }
            else this.option.format = datetimeformat;
        }
        delete this.option["dateOnly"];
        const session_data = authentication.getSessionData();
        if (session_data) {
            if (session_data["current_lang"]) {
                this.option.locale = session_data["current_lang"];
            }
        }
        this.fn = fn;
    }
    bind() {
        this.render();
        return this;
    }
    getValue() {
        return this.element.val();
    }
    setValue(value) {
        this.element.val(value);
    }
    setMinDate(value) {
        this.element.data("DateTimePicker").minDate(value);
    }
    setMaxDate(value) {
        this.element.data("DateTimePicker").maxDate(value);
    }
    render() {
        this.element.addClass("dateField");
        this.element.datetimepicker(this.option).next().on("click", () => {
            this.element.focus();
        }).unbind("dp.change").on("dp.change", () => {
            this.fn(this);
        });
    }
}