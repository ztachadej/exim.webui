﻿export default class MainMenu {
    constructor(sessionData) {
        this.sessionData = sessionData;
    }
    getMainMenuHtml() {
        if (this.sessionData.menuHtml) {
            return this.sessionData.menuHtml;
        }
        else {
            return this.generateMainMenu();
        }
    }
    generateMainMenu() {
        let menuHtml = "";
        if (this.sessionData.user) {
            if (this.sessionData.user.menus) {
                this.sessionData.user.menus.forEach(function (value, i) {
                    menuHtml += new MainMenu().genMenu(value);
                });
            }
        }
        return menuHtml;
    }
    genMenu(value) {
        let menuItemHtml = "";
        if (value.subMenu) {
            if (value.subMenu.length > 0) {
                menuItemHtml += '<li><a href="#' + value.menuId + '" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" lang="en"> <i class="' + value.menuIcon + '"></i>' + value.menuName + '</a>';
                menuItemHtml += "<ul class='collapse list-unstyled' id='" + value.menuId + "'>";
                value.subMenu.forEach(function (value, i) {
                    menuItemHtml += new MainMenu().genMenu(value);
                });
                menuItemHtml += "</li></ul>";
            }
            else {
                menuItemHtml += '<li><a href="' + value.menuUrl + '" lang="en"><i class="' + value.menuIcon + '"></i>' + value.menuName + '</a></li>';
            }
        }
        else {
            menuItemHtml += '<li><a href="' + value.menuUrl + '" lang="en"><i class="' + value.menuIcon + '"></i>' + value.menuName + '</a></li>';
        }
        return menuItemHtml;
    }
}