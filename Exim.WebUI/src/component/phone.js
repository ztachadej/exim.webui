﻿import { phonemarkformat } from "../config/config";
export default class PhoneField {
    constructor(id, option, fn) {
        this.id = id;
        this.element = $("#" + id);
        this.option = option ? option : { };
        this.fn = fn;
    }
    bind() {
        this.render();
        return this;
    }
    getValue() {
        return this.element.val();
    }
    setValue(value) {
        this.element.val(value);
    }
    render() {
        this.element.addClass("phoneField");
        this.element.unbind("blur");
        this.element.unbind("change");
        this.element.mask(phonemarkformat);
        this.element.on("change", () => {
            typeof this.fn === "function" ? this.fn(this.element) : "";
        });
    }
}