﻿export default class NumberField {
    constructor(id, option, fn) {
        this.id = id;
        this.element = $("#" + id);
        this.option = option ? option : { decimal: 0 };
        this.fn = fn;
    }
    bind() {
        this.render();
        return this;
    }
    getValue() {
        return this.element.val();
    }
    setValue(value) {
        this.element.val(value);
    }
    render() {
        this.element.addClass("numberfield").val(parseFloat("0").toFixed(this.option.decimal));
        this.element.unbind("blur");
        this.element.unbind("change");
        this.element.blur(() => {
            this.setValue(parseFloat(this.getValue().replace(",", "")).toFixed(this.option.decimal) === "NaN" ? parseFloat("0").toFixed(this.option.decimal) : parseFloat(this.getValue().replace(",", "")).toFixed(this.option.decimal).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        }).on("change", () => {
            typeof this.fn === "function" ? this.fn(this.element) : "";
        });
    }
}