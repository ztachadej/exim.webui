﻿import commonUtils from "../../src/common/commonUtils";
import formUtils from "../../src/common/formUtils";
import authentication from "../../src/common/authentication";
import { messageLang } from "../config/messageLang";
import dropdown from "../../src/component/dropdown";
import datepicker from "../../src/component/datepicker";
import numberField from "../../src/component/number";
import phoneField from "../../src/component/phone";
import datatable from "../../src/component/datatable";
import datatablegroup from "../../src/component/datatablegroup";
class BasePageHandler {
    constructor() {
        this.commonUtils = commonUtils;
        this.formUtils = formUtils;
        this.messageLang = messageLang;
        this.dropdown = dropdown;
        this.datepicker = datepicker;
        this.numberField = numberField;
        this.phoneField = phoneField;
        this.datatable = datatable;
        this.datatablegroup = datatablegroup;
    }
    validate(opt) {
        opt.errorPlacement = function (error, element) {
            const placement = $(element).data('error');
            if (placement) {
                $(placement).append(error);
            } else {
                if (element.hasClass("dateField")) {
                    error.insertAfter(element.parent());
                }
                else if (element.prop("tagName") === "SELECT") {
                    error.insertAfter(element.next());
                }
                else {
                    error.insertAfter(element);
                }
            }
        };
        opt.highlight = function (element) {
            if ($(element).prop("tagName") === "SELECT") {
                $(element).next().removeClass('error success').addClass('error');
            }
            else {
                $(element).removeClass('error success').addClass('error');
            }
        };
        opt.success = function (element) {
            if ($(element).prev().hasClass("chosen-container")) {
                $(element).prev().removeClass('error success').addClass('success');
            }
            else {
                $(element).removeClass('error success').addClass('success');
            }
        };
        $("#mainForm").validate(opt);
    }
    valid() {
        return $("#mainForm").valid();
    }
    getPageData(dataModel) {
        for (const key in dataModel) {
            const element = $("#" + key);
            if (element.hasClass("numberfield")) {
                dataModel[key] = element.val().replace(/,/g, '');
            }
            else if (element.hasClass("phoneField")) {
                dataModel[key] = element.val().replace(/-/g, '');
            }
            else if (element.hasClass("dateField")) {
                const session_data = authentication.getSessionData();
                if (session_data) {
                    if (session_data["current_lang"] === "th") {
                        const date_time = element.val().split("_");
                        const date_split = date_time[0].split("/");
                        dataModel[key] = `${date_split[0]}/${date_split[1]}/${parseInt(date_split[2]) - 543} ${date_time[1]}`;
                    }
                    else
                        dataModel[key] = element.val();
                }
                else
                    dataModel[key] = element.val();
            }
            else if (element.hasClass("table")) {
                let rowData = [];
                element.DataTable().rows().data().each(function (item) { rowData.push(item); });
                dataModel[key] = rowData;
            }
            else {
                dataModel[key] = element.val();
            }
        }
        return dataModel;
    }
    setPageData(dataModel) {
        for (const key in dataModel) {
            const element = $("#" + key);
            if (element.hasClass("table")) {
                const dt = element.DataTable();
                dt.clear();
                dt.rows.add(dataModel[key]);
                dt.draw();
            }
            else {
                element.val(dataModel[key]);
                if (element.hasClass("phoneField")) {
                    element.trigger('input');
                }
                else if (element.prop("tagName") === "SELECT") {
                    element.trigger("chosen:updated");
                }
                else if (element.attr("type") === "checkbox" || element.attr("type") === "radio") {
                    element.prop("checked", dataModel[key]);
                }
            }
        }
    }
}

export default BasePageHandler;