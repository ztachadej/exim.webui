﻿export default class Dropdown {
    constructor(id, dataSource, option, fn) {
        this.id = id;
        this.option = option ? option : {};
        this.option.width = "100%";
        this.option.displayValue = this.option.displayValue ? this.option.displayValue : "value";
        this.option.displayText = this.option.displayText ? this.option.displayText : "text";
        this.dataSource = dataSource;
        this.fn = fn;
    }
    bind() {
        this.render();
        return this;
    }
    getValue() {
        return $("#" + this.id).val();
    }
    getSelectedItem() {
        return this.dataSource.filter((x) => { return x[this.option.displayValue] === $("#" + this.id).val(); });
    }
    setValue(value) {
        $("#" + this.id).val(value);
        $("#" + this.id).trigger("chosen:updated");
    }
    setDataSource(ds) {
        this.dataSource = ds;
        this.render();
    }
    render() {
        let $select_elem = $("#" + this.id);
        $select_elem.html("<option></option>");
        this.dataSource.forEach((value, i) => {
            $select_elem.append('<option value="' + value[this.option.displayValue] + '">' + value[this.option.displayText] + '</option>');
        });
        $select_elem.chosen(this.option);
        $select_elem.trigger("chosen:updated");
        $select_elem.unbind("change").on("change", () => {
            $("#" + this.id).blur();
            typeof this.fn === "function" ? this.fn(this.getSelectedItem()) : "";
        });
    }
}