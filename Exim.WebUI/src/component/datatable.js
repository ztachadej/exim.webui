﻿import CommonUtils from "../common/commonUtils";
import * as msgLang from "../config/messageLang";
export default class DataTable {
    constructor(id, option, paramfn) {
        this.id = id;
        this.element = $("#" + id);
        this.option = option ? option : {};
        this.paramfn = paramfn;
        this.option.language = {
            "emptyTable": CommonUtils.getMessageLang(msgLang.messageLang.emptyTable),
            "info": CommonUtils.getMessageLang(msgLang.messageLang.info),
            "infoEmpty": CommonUtils.getMessageLang(msgLang.messageLang.infoEmpty),
            "lengthMenu": CommonUtils.getMessageLang(msgLang.messageLang.lengthMenu),
            "loadingRecords": CommonUtils.getMessageLang(msgLang.messageLang.loadingRecords),
            "processing": CommonUtils.getMessageLang(msgLang.messageLang.processing),
            "search": CommonUtils.getMessageLang(msgLang.messageLang.search),
            "zeroRecords": CommonUtils.getMessageLang(msgLang.messageLang.zeroRecords),
            "paginate": {
                "first": CommonUtils.getMessageLang(msgLang.messageLang.paginateFirst),
                "last": CommonUtils.getMessageLang(msgLang.messageLang.paginateLast),
                "next": CommonUtils.getMessageLang(msgLang.messageLang.paginateNext),
                "previous": CommonUtils.getMessageLang(msgLang.messageLang.paginatePrevious)
            }
        };
        if (this.option.serverSide === true) {
            this.option.processing = true;
            this.option.ajax = {
                "url": this.option.url,
                "type": "POST",
                "contentType": 'application/json; charset=utf-8',
                "data": function (d) {
                    d = (typeof paramfn === "function" ? paramfn(d) : d);
                }
            };
        }
        this.option.order = [];
    }
    bind() {
        this.render();
        return this;
    }
    getDataSource() {
        let rowData = [];
        $("#" + id).DataTable().rows().data().each((item) => { rowData.push(item); });
        return rowData;
    }
    setDataSource(ds) {
        let dt = this.element.DataTable();
        dt.clear();
        dt.rows.add(ds);
        dt.draw();
    }
    render() {
        this.element.css("width", "100%");
        this.element.DataTable(this.option);
    }
}