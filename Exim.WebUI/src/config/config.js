﻿export const langCache = true;
export const reLoginPopup = true;
export const phonemarkformat = "000-000-0000";
export const datetimeformat = "DD/MM/YYYY HH:mm";
export const dateformat = "DD/MM/YYYY";