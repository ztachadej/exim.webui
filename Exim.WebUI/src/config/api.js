﻿export const timeout = 60000;
export const timeoutError = 'Connection Timeout Please Try Again';
export const status = {
    OK: 200
};
export const apiHost = "http://localhost:51737/api";
export const apiLogin = `${apiHost}/values`;
export const apiHome = `${apiHost}/values`;
