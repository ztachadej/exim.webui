﻿import BaseService from './baseservice';
import { apiLogin } from "../../src/config/api";
export default class AccountService extends BaseService {
    constructor() {
        super();
    }
    login(username, password) {
        return this.CommonUtils.callAPIPost(apiLogin, { username: username, password: password });
    }
}