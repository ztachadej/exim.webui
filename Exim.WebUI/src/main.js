﻿import formUtils from "./common/formUtils";
import langUtils from "./common/langUtils";
import authentication from "./common/authentication";
import MainMenu from './component/mainmenu';
import CommonUtils from "../src/common/commonUtils";
$.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" });
$.validator.setDefaults({ ignore: ":hidden:not(.form-control-chosen)" });
$.validator.addMethod('phone', function (value, element) {
    return this.optional(element) || /^\d{3}-\d{3}-\d{4}$/.test(value);
}, "Please enter a valid phone number");
if (!authentication.checkAuthen())
    authentication.redirectToLogin();
langUtils.initialize();
let sessionData = authentication.getSessionData();
if (!sessionData.menuHtml) {
    sessionData.menuHtml = new MainMenu(sessionData).getMainMenuHtml();
    authentication.setSessionData(sessionData);
}
$(document).ready(() => {
    let funcLoad = setTimeout(() => {
        if (!CommonUtils.isObjectEmpty(window.lang.pack)) {
            $("#mainMenu").html(sessionData.menuHtml);
            $("#profilebtn").text(sessionData.user.name);
            $('#sidebarCollapse').on('click', () => {
                $('#sidebar').toggleClass('active');
            });
            $("#logoutbtn").on('click', () => { authentication.logout(); });
            var loadPromise = new Promise((resolve, reject) => {
                typeof (window.pageLoad) === "function" ? window.pageLoad(resolve) : resolve();
            });
            loadPromise.then((value) => {
                formUtils.hideLoading();
            });
            clearTimeout(funcLoad);
        }
        else {
            funcLoad();
        }
    }, 100);
});
