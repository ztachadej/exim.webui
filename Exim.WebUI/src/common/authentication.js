﻿import CommonUtils from "./commonUtils";
import formUtils from "./formUtils";
import AccountService from "../../src/services/accountservice";
export default class Authentication {
    constructor() {
        this.accountService = new AccountService();
    }
    static getSessionData() {
        return localStorage.getItem("session_data") !== null ? JSON.parse(localStorage.getItem("session_data")) : null;
    }
    static setSessionData(session_data) {
        localStorage.setItem("session_data", JSON.stringify(session_data));
    }
    static checkAuthen() {
        if (this.getSessionData() === null) return false;
        else return true;
    }
    static redirectToLogin() {
        CommonUtils.redirectTo(`${CommonUtils.getHostUrl()}/Account/Login`);
    }
    static setAccessToken(access_token) {
        let session_data = this.getSessionData();
        session_data["access_token"] = access_token;
        localStorage.setItem("session_data", session_data);
    }
    static showLogin() {
        $("#loginbtn").unbind("click");
        $("#loginbtn").on('click', () => {
            let username = $("#usernametxt").val();
            let password = $("#passwordtxt").val();
            if (username && password) {
                this.login(username,password);
            }
            else {
                formUtils.alertWarning("Please enter a username and password.");
            }
        });
        $('#modalLogin').modal({
            backdrop: 'static',
            keyboard: false
        });
    }
    static removeSession() {
        localStorage.removeItem("session_data");
    }
    static async login(username,password) {
        let result = await this.accountService.login(username, password);
        this.loginResult(result);
    }
    static logout() {
        this.removeSession();
        this.redirectToLogin();
    }
    static loginResult(resp) {
        if (resp) {
            if (resp.status === status.OK) {
                this.setSessionData(resp.result);
                $('#modalLogin').modal("hide");
            }
            else {
                formUtils.alertFailed("Username or password is incorrect");
            }
        }
        else
            authentication.removeSession();
    }
}