﻿import authentication from "./authentication";
import * as cacheConfig from '../config/config';
export default class LangUtils {
    constructor() {
    }
    static initialize() {
        window.lang = new Lang();
        let sessionData = authentication.getSessionData();
        let uniqueUrl = "";
        if (cacheConfig.langCache === true) {
            uniqueUrl = `?_=${new Date().getTime()}`;
        }
        window.lang.dynamic('th', `/lib/jquery-lang/js/langpack/th.json${uniqueUrl}`);
        window.lang.dynamic('eng', `/lib/jquery-lang/js/langpack/en.json${uniqueUrl}`);
        window.lang.init({
            currentLang: sessionData === null ? "eng" : sessionData["current_lang"]
        });
    }
}