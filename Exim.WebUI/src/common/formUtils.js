﻿import CommonUtils from "./commonUtils";
import * as msgLang from "../config/messageLang";
export default class FormUtils {
    constructor() {
    }
    static validate() {
        let checkValid = true;
        let msg = "";
        $(".require").each((index, element) => {
            if (!$(element).val()) {
                msg += msg !== "" ? "<br/>" : "";
                msg += CommonUtils.getMessageLang(msgLang.messageLang.requireMsg).replace("{0}", $("label[for='" + $(element).attr("id") + "']").text());
                checkValid = false;
            }
        });
        $(".number").each((index, element) => {
            if (!CommonUtils.isNumber($(element).val())) {
                msg += msg !== "" ? "<br/>" : "";
                msg += CommonUtils.getMessageLang(msgLang.messageLang.numberMsg).replace("{0}", $("label[for='" + $(element).attr("id") + "']").text());
                checkValid = false;
            }
        });
        $(".email").each((index, element) => {
            if (!CommonUtils.isEmail($(element).val())) {
                msg += msg !== "" ? "<br/>" : "";
                msg += CommonUtils.getMessageLang(msgLang.messageLang.emailMsg).replace("{0}", $("label[for='" + $(element).attr("id") + "']").text());
                checkValid = false;
            }
        });
        msg !== "" ? this.alertWarning(msg) : "";
        return checkValid;
    }
    static hideAlert() {
        $(".alert").fadeOut("fast");
        clearTimeout(window.showAlertTimeout);
    }
    static showAlert() {
        $(".alert").fadeIn("fast");
        window.showAlertTimeout = setTimeout(() => {
            this.hideAlert();
        }, 5000);
    }
    static alertSuccess(msg) {
        this.hideAlert();
        $(".alert").removeClass("alert-warning").removeClass("alert-danger").addClass("alert-success");
        $(".alert").html("<strong>Success!</strong><br/> " + msg + "<button type= 'button' class= 'close no-outline' aria-label='Close' onclick='$(\".alert\").fadeOut(\"fast\");'><span aria-hidden='true'>&times;</span></button>");
        this.showAlert();
    }
    static alertWarning(msg) {
        this.hideAlert();
        $(".alert").removeClass("alert-success").removeClass("alert-danger").addClass("alert-warning");
        $(".alert").html("<strong>Warning!</strong><br/> " + msg + "<button type= 'button' class= 'close no-outline' aria-label='Close' onclick='$(\".alert\").fadeOut(\"fast\");'><span aria-hidden='true'>&times;</span></button>");
        this.showAlert();
    }
    static alertFailed(msg) {
        this.hideAlert();
        $(".alert").removeClass("alert-success").removeClass("alert-warning").addClass("alert-danger");
        $(".alert").html("<strong>Error!</strong><br/> " + msg + "<button type= 'button' class= 'close no-outline' aria-label='Close' onclick='$(\".alert\").fadeOut(\"fast\");'><span aria-hidden='true'>&times;</span></button>");
        this.showAlert();
    }
    static showAlertModal(msg) {
        $("#modalAlertTitle").text(CommonUtils.getMessageLang(msgLang.messageLang.titleAlert));
        $("#modalAlertMsg").html(msg);
        $("#modalBtnConfirm").hide();
        $("#modalAlert").modal("show");
    }
    static showConfirmModal(msg, fn) {
        $("#modalAlertTitle").text(CommonUtils.getMessageLang(msgLang.messageLang.titleConfirm));
        $("#modalAlertMsg").html(msg);
        $("#modalBtnConfirm").unbind("click");
        $("#modalBtnConfirm").click(() => { fn(); this.hideAlertModal(); }).show();
        $("#modalAlert").modal("show");
    }
    static hideAlertModal() {
        $("#modalAlert").modal("hide");
    }
    static showLoading() {
        $(".overlay").css("display", "block");
    }
    static hideLoading() {
        $(".overlay").css("display", "none");
    }
}