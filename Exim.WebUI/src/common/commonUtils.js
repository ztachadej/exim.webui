﻿import axios from 'axios';
import * as apiConfig from '../config/api';
import authentication from "./authentication";
import formUtils from "./formUtils";
import * as config from '../config/config';
export default class CommonUtils {
    constructor() {
        this.regexNumber = /^\d+$/;
        this.regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    }
    static callAPIGet(url, param) {
        formUtils.showLoading();
        if (param === null) {
            return this.to(this.getAxios().get(url));
        }
        else
            return this.to(this.getAxios().get(url, { params: param }));
    }
    static callAPIPost(url, param) {
        formUtils.showLoading();
        return this.to(this.getAxios().post(url, param));
    }
    static callbackSuccess(fn, res) {
        fn(res);
        formUtils.hideLoading();
    }
    static callbackError(res) {
        formUtils.alertFailed(res);
        formUtils.hideLoading();
    }
    static convertDateString(dateString, toFormat) {
        return moment(dateString).format(toFormat);
    }
    static getCurrentDateStr(format) {
        return moment().format(format);
    }
    static getAxios() {
        let sessionData = authentication.getSessionData();
        return axios.create({
            baseURL: apiConfig.apiHost,
            timeout: apiConfig.timeout,
            headers: {
                'Authorization': sessionData === null ? "" : sessionData["access_token"],
                'Cache-Control': "no-cache",
                'current_lang': sessionData === null ? "eng" : sessionData["current_lang"]
            }
        });
    }
    static to(promise) {
        return promise.then(data => {
            formUtils.hideLoading();
            return {
                status: data.status,
                error: null,
                result: data.data
            };
        }).catch(err => {
            formUtils.hideLoading();
            if (err.response.status === 401) {
                if (config.reLoginPopup === true) {
                    authentication.showLogin();
                }
                else {
                    authentication.redirectToLogin();
                }
            }
            else {
                formUtils.alertFailed(err);
            }
            return null;
        });
    }
    static isNumber(value) {
        return new CommonUtils().regexNumber.test(value);
    }
    static isEmail(value) {
        return new CommonUtils().regexEmail.test(value.toLowerCase());
    }
    static isEmpty(value) {
        return !value;
    }
    static isObjectEmpty(value) {
        for (let key in value) {
            if (value.hasOwnProperty(key))
                return false;
        }
        return true;
    }
    static isArrayEmpty(value) {
        if (value !== null) {
            if (value.length > 0) {
                return false;
            }
        }
        return true;
    }
    static getHostUrl() {
        return window.location.origin;
    }
    static redirectTo(url) {
        return window.location.href = `${url}?_${new Date().getTime()}`;
    }
    static getMessageLang(langid) {
        return window.lang.pack[window.lang.currentLang === "en" ? "eng" : window.lang.currentLang]["token"][langid];
    }
}